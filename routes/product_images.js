var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
//const resizeImg = require('resize-img');
const Joi = require('joi');
const sharp = require('sharp');
const fsExtra = require('fs-extra');



var storage = multer.diskStorage({
    destination: function (req, file, cb) 
    {
      fsExtra.emptyDirSync('Images/');
      console.log('Images Directory Empty');
      fs.mkdirSync('Compressed', { recursive: true })
      cb(null, 'Images/')
    },
    filename: function (req, file, cb) {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});


// Upload a photo
//    app.js       /productImages
router.post('/', ensureToken, upload.array('image',10), function (req, res, next) 
{
  const lenght = req.files.length;
  for( let i = 0; i < lenght ; i++ )
  {
      var image_name = req.files[i].filename;
      var id = 0;
      var dimensions = sizeOf('Images/'+image_name);
      var actual_width = dimensions.width;
      var actual_height =dimensions.height;
      var reduced_height,reduced_width;
 
      if(actual_height>actual_width)
      {
        console.log("height greater"); 
        reduced_width=180;
        var aspect_ratio =  actual_height / actual_width ;
        reduced_height = reduced_width * aspect_ratio
      }
      else if(actual_height<actual_width)
      {
        console.log("width greater");
        reduced_height=180;
        var aspect_ratio =  actual_width / actual_height ;
        reduced_width = reduced_height * aspect_ratio
      }
      else if(actual_height==actual_width)
      {
        console.log("width and height is equal");
        reduced_height=180;
        reduced_width = 180;
      }
  
  sharp('Images/'+image_name)
  .resize(parseInt(reduced_width), parseInt(reduced_height))
  .toFile('Compressed/'+image_name);
  console.log('Image Resized!');
 
 
  var user_id = req.query.id;
  
 
  pool.query("INSERT INTO product_images value (?,?,?)",
  [ id, req.files[i].filename ,user_id], function(err, row, fields) 
  {
         if(err) 
         {
           console.log(err);
           res.send('Error While Uploading File : ' + req.files[i].filename + ' ' + err);
           return;
         }
         else if(i==(lenght-1))
         {
           res.send('Product Image Added');
           console.log('Product Image Added');
         }
  }   
  )}
  
  fsExtra.emptyDir('Images/',(err)=>
  {
    if(!err)
    {
       console.log('Images Directory Empty');
    }
    else
    {
       console.log('Following Error Occured WHile Deleting Files From Images/ : ' + err);
    }
  });


});


router.get('/getimage/:page',ensureToken, function(request, response) 
{
   var page = req.params.page;
   let startNum = (page - 1) * 20;
   let LimitNum = 20;
   pool.query(`select * from product_images ORDER BY Id limit ${LimitNum} OFFSET ${startNum} `, 
   function(err, result) 
   {
      response.send(result); // Send the image to the browser.
      console.log("Image Displayed!");
    
   });
});



router.get('/getimage/:id',ensureToken, function(request, response) 
{

  // fetch the request data
 const data = req.params;

 // define the validation schema
  const schema = Joi.object().keys({
 
 
      id: Joi.number().integer().min(1).max(2000),
 
     
 
  });
 
  // validate the request data against the schema
  Joi.validate(data, schema, (err, value) => {
 
      // create a random number as id
      //const id = Math.ceil(Math.random() * 9999999);
 
      if (err) {
          // send a 422 error response if validation fails
          res.status(422).json({
              status: 'error',
              message: 'Invalid request data',
              data: data
          });
          console.log("INvalid REquest Data")
      } else {
 
 
  // else part










  var id = request.params.id;
  pool.query(`select * from product_images WHERE id =${id}`, function(err, result) {
    //  response.writeHead(200, {
     //     'Content-Type': 'image/jpeg'
     // });
      //console.log("HELLO");
      response.send(result); // Send the image to the browser.
      console.log("Image Displayed!");
    //  response.json({status : "OK"})
    });
      }
  })

  });




function ensureToken(req,res,next){
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined")
  {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
  }else {
      res.sendStatus(403);

  }
}




module.exports = router;

