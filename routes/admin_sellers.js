const router = require("../users/signIn");
var express = require('express');
var jwt = require('jsonwebtoken');
var db = require('../db');
var bodyParser = require('body-parser');
const { json } = require('body-parser');
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({extended: false})); // for parsing application/x-www-form-urlencoded
const Joi = require('joi');

// ADMIN Getting A List Of Sellers


// app.js   /adminsellers

router.get('/allSellers/:page',ensureToken,(req,res,next)=>
{

    
    jwt.verify(req.token,'my_secret_key',function(err,data)
    {{

        if(err)
        {
            res.status(403).send({ error: 'Access Denied' });
            console.log('Access Denied');
        }
        else
        {
            var page = req.params.page;
            var limit = 20;
            var startNum = (page -1) * 20;
            var sql = `SELECT * FROM customers WHERE Status="seller" 
            ORDER BY Id limit ${limit} OFFSET ${startNum}`;
            db.query(sql, function(err, result) 
            {
              if(err) 
              {
                 console.log('Following Error Occured  : ' + err);
                 res.status(500).send({ err });
              }
              else
              {
                res.json({result});
                console.log('20 Sellers DIsplayed on Page No : ' + page);
              }
        })
    }
    }})   

});


//  Getting A Specific  Seller
//     /adminsellers
router.get('/:id',ensureToken,(req,res,next)=>
{    
// fetch the request data
 const data = req.body;
 // define the validation schema
  const schema = Joi.object().keys
  ({
     id: Joi.number().integer().min(1).max(2000),
  });
 
  // validate the request data against the schema
  Joi.validate(data, schema, (err, value) => 
  {
      if (err) 
      {
          res.status(422).json
          ({
              status: 'error',
              message: 'Invalid request data',
              data: data,
              error : err
          });
      } 
      else 
      {
 
        jwt.verify(req.token,'my_secret_key',function(err,data){{        
        if(err)
        {
            res.status(403).send({ error: 'Access Denied' })
        }
        else
        {
          var id = req.params.id;
          var sql = `SELECT * FROM customers WHERE Id=${id}`;
          db.query(sql, function(err, row, fields) {
          if(err) 
          {
            res.status(500).send({ error : 'Following Error Occured : ' + err });
            console.log('Following Error Occured ');
          }
          else
          {
            res.json(row);
            console.log(row);
          }
              
              
        })
        }
        }}) 
    }
    })
    });


//   Admin Deleting A Seller
//    /adminsellers
router.delete('/delSeller/:id',ensureToken,(req,res,next)=>
{
       
// fetch the request data
 const data = req.body;

 // define the validation schema
  const schema = Joi.object().keys
  ({
     id: Joi.number().integer().min(1).max(2000),
   });
 
  // validate the request data against the schema
  Joi.validate(data, schema, (err, value) => 
  {
     
      if (err) 
      {
         res.status(422).json
          ({
              status: 'error',
              message: 'Invalid request data',
              data: data,
              error : err
          });
      } 
      else 
      {
         jwt.verify(req.token,'my_secret_key',function(err,data)
         {{        
            if(err)
            {
                res.status(403).send({ error: 'Access Denied' })
            }
            else
            {
                var id = req.params.id;
                var sql = `DELETE FROM customers WHERE Id=${id} AND Status = "seller"`;
                db.query(sql, function(err, result) 
                {
                  if(err) 
                  {
                    res.status(500).send({ err })
                  }
                  else
                  {
                    res.json({'status': 'Deleted'});
                    console.log('Deleted');
                  }
                })
            }
            }})
      }
        })
    });


function ensureToken(req,res,next)
{
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined")
    {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else 
    {
        res.sendStatus(403);
    }
}

module.exports = router ;