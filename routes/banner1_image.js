var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
//const resizeImg = require('resize-img');
const Joi = require('joi');
const sharp = require('sharp');
const fsExtra = require('fs-extra');



var storage = multer.diskStorage
({
    destination: function (req, file, cb) 
    {
      fsExtra.emptyDirSync('Images/');
      console.log('Images Directory Empty');
        fs.mkdirSync('Compressed', { recursive: true })
        cb(null, 'Images/')
    },
    filename: function (req, file, cb) 
    {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
 })

var upload = multer
({
    storage: storage
});


// Upload a photo

// app.js    /banner1image



router.post('/', ensureToken ,upload.single('image'), function (req, res, next) 
{

     var image_name = req.file.filename;
     var id = 0;
     var dimensions = sizeOf('Images/'+image_name);
     var actual_width = dimensions.width;
     var actual_height =dimensions.height;
     var reduced_height,reduced_width;

     if(actual_height>actual_width)
     {
       console.log("height greater"); 
       reduced_width=180;
       var aspect_ratio =  actual_height / actual_width ;
       reduced_height = reduced_width * aspect_ratio
     }
     else if(actual_height<actual_width)
     {
       console.log("width greater");
       reduced_height=180;
       var aspect_ratio =  actual_width / actual_height ;
       reduced_width = reduced_height * aspect_ratio
     }
     else if(actual_height==actual_width)
     {
       console.log("width and height is equal");
       reduced_height=180;
       reduced_width = 180;
     }
 
 sharp('Images/'+image_name)
 .resize(parseInt(reduced_width), parseInt(reduced_height))
 .toFile('Compressed/'+image_name,(err)=>{
   if(!err)
   {
    fsExtra.emptyDir('Images/');
    console.log('Inside toFile and IMage Directory Empty');
   }
 });
 console.log('Image Resized!');


 var user_id = req.query.id;

 pool.query("INSERT INTO banner_images value (?,?,?)", 
 [ id, req.file.filename ,user_id], function(err, row, fields) 
 {
        if(err) 
        {
          console.log(err);
          res.send('Error While Uploading File : ' + req.file.filename + ' ' + err);
          return;
        }
        else 
        {
          res.send('Banner Image Added');
          console.log('Banner Image Added');
        }
 }   
 )
    
  
});

//app.js   /banner1image

router.get('/getimage/:page',ensureToken ,function(request, response) 
{
  
  var page = request.params.page;
  var limit = 20;
  var startNum = (page -1) * limit;
  pool.query(`select * from banner_images ORDER BY Image_Id limit ${limit} OFFSET ${startNum} `, 
  function(err, result) 
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      response.send('Following Error Occcured : ' + err);
    }
      response.send(result); // Send the image to the browser.
      console.log("Image Displayed")
      
  });
});


router.get('/getSingleimage/:id',ensureToken ,function(request, response) 
{

 const data = request.body;
 // define the validation schema
 const schema = Joi.object().keys(
   {
      id: Joi.number().integer().min(1).max(2000),
   });
 
  // validate the request data against the schema
  Joi.validate(data, schema, (err, value) => 
  {
      if (err) 
      {
          res.status(422).json(
            {
              status: 'error',
              message: 'Invalid request data',
              error: err
            });
          console.log('Invalid Request Data : ' + err);
      } 
      else 
      {
         var id = request.params.id;
         pool.query(`select * from banner_images WHERE Image_Id=${id} `, 
         function(err, result) 
         {
           if(!err)
           {
            response.send(result); // Send the image to the browser.
            console.log(result);
           }
           else
           {
             console.log('Following Error Occured : ' + err);
             res.send('Following Error Occured : ' + err);
           }
                   });
      }
  })
});



function ensureToken(req,res,next)
{
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined")
  {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
  }
  else 
  {
      res.sendStatus(403);
  }
}




module.exports = router;

